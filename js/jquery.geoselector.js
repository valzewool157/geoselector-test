;(function ($) {

  var widget;

  var geoSelectorTemplate = '<div class="geoselector-left-col"> \
        <label>Введите название региона</label> \
        <input type="text" class="geoselector-input"> \
        <label>Выберите регион:</label> \
        <ul class="geoselector-regions"></ul> \
      </div> \
      <div class="geoselector-right-col"> \
        <label>Города в выбранном регионе:</label> \
        <ul class="geoselector-cities"></li> \
      </div>';

  var defaults = {
    regionID: null,
    minInputChar: 1,
    bindEvent: 'keyup',
    events: {}
  }

  var methods = {
    'set': function (property, value) {
      this.config[property] = value;
    },
    'get': function (property) {
      var value = this.config[property];
      return value;
    }
  }

  var GeoSelector = function (element, options) {
    var self = this;

    self.element = element;

    self.config = $.extend(true, {}, defaults);
    if (typeof options === 'number') {
      self.config.regionID = options;
    } else if (typeof options === 'object') {
      $.extend(true, self.config, options);
    }

    //add handler for calling renderRegionsList after delay
    self.element.on(self.config.bindEvent, function (e) {
      clearTimeout(self.timeout);
      self.timeout = setTimeout(function () {
        var name = $(e.target).val();
        self.renderRegionslist(name);
      }, 500);
    })

    //add handlers to user events
    $.each(self.config.events, function (key, value) {
      if (typeof value === 'function') {
        self.element.on(key + '.geoselector', function (event, data) {
          value(data);
        });
      }
    })

    self.init();
  }

  //initialize
  GeoSelector.prototype.init = function () {
    this.render();

    //render cities if there is regionID by default or as option
    if (this.config.regionID) {
      this.renderCitiesList(this.config.regionID);
    }
  }

  //render widget
  GeoSelector.prototype.render = function () {
    var $el = this.element;

    $el.addClass('geoselector-container');
    $el.html(geoSelectorTemplate);
  }

  //render regions list
  GeoSelector.prototype.renderRegionslist = function (name) {
    var self = this,
        $cities = self.element.find('.geoselector-cities'),
        $regions = self.element.find('.geoselector-regions');

    $regions.html('');
    $cities.html('');
    if (name && name.length >= self.config.minInputChar) {
      self.getRegionsList(name, function (err, data) {
        if (!err) {
          self.element.trigger('onGsData.geoselector', [data]);
          data.forEach(function (region) {
            var regionCode = region[0],
                regionName = region[1];

            var regionItem = '<li><a class="region-item" data-code="' + regionCode + '">' + regionName + '</a></li>';
            $regions.append(regionItem);
          })

          //add click handler for rendered regions
          self.element.find('.region-item').on('click', function (e) {
            var code = $(e.target).data().code;
            
            self.renderCitiesList(code);
          })
        } else {
          self.element.trigger('onGsError.geoselector', [err]);
        }
      })
    }
  }

  //render cities list
  GeoSelector.prototype.renderCitiesList = function (code) {
    var self = this,
        $cities = self.element.find('.geoselector-cities');

    $cities.html('');
    if (code) {
      self.element.trigger('onGsCityStart.geoselector');
      self.getCitiesList(code, function (err, data) {
        if (!err) {
          self.element.trigger('onGsCityData.geoselector', [data]);
          data.forEach(function (city) {
            var cityName = city[1];

            var cityItem = '<li class="city-item">' + cityName + '</li>';
            $cities.append(cityItem);
          })
        } else {
          self.element.trigger('onGsError.geoselector', [err]);
        }
      })
    }
  }

  //API request for regions
  GeoSelector.prototype.getRegionsList = function (name, cb) {
    $.ajax({
      method: 'GET',
      url: 'http://evildevel.com/Test/Region',
      data: {
        name: name
      },
      error: function (xhr, status, thrownError) {
        cb(status, []);
      },
      success: function (data) {
        cb(null, data);
      }
    })
  }

  //API request for cities
  GeoSelector.prototype.getCitiesList = function (code, cb) {
    $.ajax({
      method: 'GET',
      url: 'http://evildevel.com/Test/City',
      data: {
        region: code
      },
      error: function (xhr, status, thrownError) {
        cb(status, []);
      },
      success: function (data) {
        cb(null, data);
      }
    })
  }

  $.fn.geoSelector = function (options) {
    if (methods[options]) {
      return methods[options].apply(widget, Array.prototype.slice.call( arguments, 1 ));
    } else if (!options || typeof(options) === 'number' || typeof(options) === 'object') {
      widget = new GeoSelector(this.first(), options);
      return this.first();
    }
  }
})(jQuery);